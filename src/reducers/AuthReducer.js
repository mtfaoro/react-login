export const AuthReducer = (state, action) => {
  switch (action.type) {
    case 'AUTH':
      return {...state, isAuthenticated : true} 
    case 'LOGOUT':
      return {...state, isAuthenticated : false}
    case "LOGIN":
      return {...state, isRegister : false}
    case "REGISTER":
      return {...state, isRegister : true}
    default:
      return state;
  }
} 