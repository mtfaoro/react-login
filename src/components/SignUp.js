import React, { useState, useContext } from "react";
import {HelpBlock, FormGroup, FormControl, ControlLabel} from "react-bootstrap";
import { Auth } from "aws-amplify";
import LoaderButton from "./LoaderButton";
import { AuthContext } from "../contexts/AuthContext";

//import "./Signup.css";

const SignUp = () =>{

    const {auth, dispatch} = useContext(AuthContext)
    const [isLoading, setIsLoading] = useState(false)
    const [user, setCredentials] = useState({email : "", 
                                             password :"",
                                             confirmPassword : "",
                                             confirmationCode: "",
                                             newUser: null})


    if(!auth.isRegister || auth.isAuthenticated){
        return null
    }                                         

    const validateForm = () =>{
        return (
            user.email.length > 0 &&
            user.password.length > 0 &&
            user.password === user.confirmPassword
          );    
    }                             
    
    
    const validateConfirmationForm = () => {
      return user.confirmationCode.length > 0;
    }

    const handleSubmit = async event => {
      event.preventDefault();
  
      setIsLoading(true)
  
      try {
        const newUser = await Auth.signUp({
          email: user.email,
          username : user.email,
          password: user.password
        });

        setCredentials({...user, newUser})
      } catch (e) {
        console.log(e);
      }

      setIsLoading(false)
    }


    const handleConfirmationSubmit = async event => {
      event.preventDefault();
  
      setIsLoading(true)
  
      try {
        await Auth.confirmSignUp(user.email, user.confirmationCode);
        await Auth.signIn(user.email, user.password);
        dispatch({ type: 'AUTH'})

       // this.props.history.push("/");
      } catch (e) {
        console.log("error", e)
        setIsLoading(false);
      }
    }


    const renderElement = (id, label, type, value , changeF) =>{
        return <FormGroup controlId={id} bsSize="large">
                    <ControlLabel>{label}</ControlLabel>
                    <FormControl
                    autoFocus
                    type={type}
                    value={value}
                    onChange={changeF}
                    />
                </FormGroup>
    }

    
    const renderForm = () => {
        return (
                <form onSubmit={handleSubmit}>
                    {renderElement("email", "Email", "email", user.email, 
                                (e) => setCredentials({...user, email : e.target.value}))}  
                    {renderElement("password", "Password", "password", user.password, 
                                (e) => setCredentials({...user, password : e.target.value}))}  
                    {renderElement("confirmPassword", "Confirm Password", "password", user.confirmPassword, 
                                (e) => setCredentials({...user, confirmPassword : e.target.value}))}  

                    <LoaderButton
                      block bsSize="large" disabled={!validateForm()}
                      type="submit"  isLoading={isLoading}
                      text="Signup"  loadingText="Signing up…"
                    />
                </form>
                );
      }

      const renderConfirmationForm = () => {
        return (
          <form onSubmit={handleConfirmationSubmit}>
           
           {renderElement("confirmationCode", "Confirmation Code", "tel", user.confirmationCode, 
                                (e) => setCredentials({...user, confirmationCode : e.target.value}))}  
                    
            <LoaderButton
              block
              bsSize="large"
              disabled={!validateConfirmationForm()}
              type="submit"
              isLoading={isLoading}
              text="Verify"
              loadingText="Verifying…"
            />
          </form>
        );
      }

      if(user.newUser === null){
        return renderForm()
      } 
      
      return renderConfirmationForm()

}
export default SignUp;




/*
export default class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: "",
      confirmPassword: "",
      confirmationCode: "",
      newUser: null
    };
  }

  validateForm() {
    return (
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.password === this.state.confirmPassword
    );
  }

  validateConfirmationForm() {
    return this.state.confirmationCode.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      const newUser = await Auth.signUp({
        username: this.state.email,
        password: this.state.password
      });
      this.setState({
        newUser
      });
    } catch (e) {
      alert(e.message);
    }

    this.setState({ isLoading: false });
  }

  handleConfirmationSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await Auth.confirmSignUp(this.state.email, this.state.confirmationCode);
      await Auth.signIn(this.state.email, this.state.password);

      this.props.userHasAuthenticated(true);
      this.props.history.push("/");
    } catch (e) {
      alert(e.message);
      this.setState({ isLoading: false });
    }
  }

  renderConfirmationForm() {
    return (
      <form onSubmit={this.handleConfirmationSubmit}>
        <FormGroup controlId="confirmationCode" bsSize="large">
          <ControlLabel>Confirmation Code</ControlLabel>
          <FormControl
            autoFocus
            type="tel"
            value={this.state.confirmationCode}
            onChange={this.handleChange}
          />
          <HelpBlock>Please check your email for the code.</HelpBlock>
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateConfirmationForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Verify"
          loadingText="Verifying…"
        />
      </form>
    );
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            value={this.state.password}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>
        <FormGroup controlId="confirmPassword" bsSize="large">
          <ControlLabel>Confirm Password</ControlLabel>
          <FormControl
            value={this.state.confirmPassword}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Signup"
          loadingText="Signing up…"
        />
      </form>
    );
  }

  render() {
    return (
      <div className="Signup">
        {this.state.newUser === null
          ? this.renderForm()
          : this.renderConfirmationForm()}
      </div>
    );
  }
}*/
