import React, { useState, useContext } from "react";
import { Auth } from "aws-amplify";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "./LoaderButton";
import "./Login.css";
import { AuthContext } from "../contexts/AuthContext";


const Login = () => {

    const {auth, dispatch} = useContext(AuthContext)
    const [isLoading, setIsLoading] = useState(false)
    const [credentials, setCredentials] = useState({email : "", password :""})

    if(auth.isRegister || auth.isAuthenticated){
        return null
    }

    Auth.currentAuthenticatedUser({bypassCache: false })
    .then(user =>  dispatch({ type: 'AUTH'}))
    .catch(err => console.log(err));

    const validateForm = () => {
       return credentials.email.length > 0 && credentials.password.length > 0;
    }

    const handleSubmit = async event => {
        event.preventDefault();
  
        setIsLoading(true)    
      
        try {
          await Auth.signIn(credentials.email, credentials.password);
          dispatch({ type: 'AUTH'})
          setIsLoading(false)
        } catch (e) {
          alert(e.message);
          setIsLoading(false)    
        }
    }

 
    return <div className="Login">
                <form onSubmit={handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl autoFocus type="email" 
                                     value={credentials.email} 
                                     onChange={(event) => setCredentials({...credentials, 
                                                                     email : event.target.value})} />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={credentials.password}
                            onChange={(event) => setCredentials({...credentials,
                                                            password : event.target.value})}
                            type="password"
                        />
                    </FormGroup>
                    <LoaderButton  block  bsSize="large"    disabled={!validateForm()}
                    type="submit"
                    isLoading={isLoading}
                    text="Login"
                    loadingText="Logging in…"
                    />
                </form>

                <button onClick={() => dispatch({ type: 'REGISTER'})}>Register</button>                
            </div>
}

export default Login