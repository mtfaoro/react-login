import React, { useState, useContext } from "react";
import { Auth, API } from "aws-amplify";
import { AuthContext } from "../contexts/AuthContext";
import LoaderButton from "./LoaderButton";

const Logout = () =>{

    const {auth, dispatch} = useContext(AuthContext)
    const [email, setEmail] = useState("")
    const [result, setResult] = useState("")

    if(!auth.isAuthenticated){
        return null 
    }

    Auth.currentAuthenticatedUser({bypassCache: false })
    .then(user =>  setEmail(user.attributes.email))
    .catch(err => console.log(err));

    const handleLogout = async event => {
        await Auth.signOut();
        dispatch({ type: 'LOGOUT'})
    }

    const callAPI = () =>{
        API.post("test", "", {})
        .then((response) =>{
            setResult(response)
        })
        .catch((error) => {
            setResult(error.stack)
        })
    }
   
    return <div>
                <h1>Hello: {email}</h1>
                <p><button onClick={callAPI}>API</button></p>
                
                {result > ""
                 ? <p>result: {result}</p>
                : null      }

                <p><button onClick={handleLogout}>Logout</button></p>
           </div>

}
export default Logout