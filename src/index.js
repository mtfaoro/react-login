import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Amplify from "aws-amplify";
import config from './config'
import * as serviceWorker from './serviceWorker';

Amplify.configure({
    Auth: {
      mandatorySignIn: true,
      region: config.AWS_REGION,
      userPoolId: config.AWS_USER_POOL,
      identityPoolId: config.AWS_IDENTITY_POOL,
      userPoolWebClientId: config.AWS_USER_POOL_WEB_CLIENT_ID
    },
    API: {
      endpoints: [
        {
          name: "test",
          endpoint: config.API_GATEWAY + "test",
          region: config.AWS_REGION        },
      ]
    }
  });

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
