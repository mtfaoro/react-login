import React, { createContext, useReducer, useEffect } from 'react';
import { AuthReducer } from '../reducers/AuthReducer';



export const AuthContext = createContext();

const AuthContextProvider = (props) => {
 
  const [auth, dispatch] = useReducer(AuthReducer, {isAuthenticated : false})

  /*useEffect(() => {
    localStorage.setItem('books', JSON.stringify(books));
  }, [books]);*/
  return (
    <AuthContext.Provider value={{ auth, dispatch }}>
      {props.children}
    </AuthContext.Provider>
  );
}
 
export default AuthContextProvider;