import React from 'react';
import './App.css';
import { Auth } from "aws-amplify";
import Login from './components/Login';
import SignUp from './components/SignUp';
import AuthContextProvider from './contexts/AuthContext';
import Logout from './components/Logout';

function App() {

    
  
  return (
    <div className="App">
      <AuthContextProvider>
          <Login/>
          <SignUp/>
          <Logout/>
      </AuthContextProvider>
    </div>
  );
}

export default App;
